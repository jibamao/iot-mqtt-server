package cn.recallcode.iot.mqtt.server.broker.protocol;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

public abstract class AbstractMessageProcessor {
    public abstract void process(Channel channel, MqttMessage mqttMsg);
}
