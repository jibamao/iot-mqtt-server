/**
 * Copyright (c) 2018, Mr.Wang (recallcode@aliyun.com) All rights reserved.
 */

package cn.recallcode.iot.mqtt.server.auth.service;

import cn.recallcode.iot.mqtt.server.common.auth.AuthResult;
import cn.recallcode.iot.mqtt.server.common.auth.IAuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;

/**
 * 用户名和密码认证服务
 */
@Slf4j
@Service
public class AuthService implements IAuthService {

	@Override
	public AuthResult checkValid(String clientId, String username, String password, String remoteIp, int port) {
		log.info("clientId={}, username={}, password={}, remoteIp={}, port={}",
				clientId, username, password, remoteIp, port);
		//TODO: add authentication logic
		return new AuthResult("fakeId", Collections.emptyList(), "");
	}

}
