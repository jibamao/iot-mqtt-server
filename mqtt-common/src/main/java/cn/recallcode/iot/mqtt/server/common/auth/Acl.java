package cn.recallcode.iot.mqtt.server.common.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Acl {
    private String topicFilter;
    private boolean canRead;
    private boolean canWrite;
}
