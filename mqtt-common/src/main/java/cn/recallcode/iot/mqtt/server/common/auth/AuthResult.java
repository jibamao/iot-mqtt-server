package cn.recallcode.iot.mqtt.server.common.auth;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthResult {
    // application id like vin or other
    private String appId;
    private List<Acl> acl;
    private String opaque;
}
